package com.abepro.marksqr;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ThirdPartyActivity extends AppCompatActivity {
    private EditText prntext;
    private String prn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third_party);
        Button scanqr = findViewById(R.id.button7);
        prntext = findViewById(R.id.editText3);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(prntext, InputMethodManager.SHOW_IMPLICIT);

        scanqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prn = prntext.getText().toString().trim();
//                Toast.makeText(getApplicationContext(),prn,Toast.LENGTH_SHORT).show();

                //Create Firebase database reference here
                DatabaseReference base = FirebaseDatabase.getInstance().getReference();
                DatabaseReference student = base.child("Students").child(prn);

                student.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        String finalprn, finalname, username, password, marks;
                        finalprn = dataSnapshot.child("prnNumber").getValue(String.class);
                        finalname = dataSnapshot.child("name").getValue(String.class);
                        username = dataSnapshot.child("username").getValue(String.class);
                        password = dataSnapshot.child("password").getValue(String.class);
                        marks = dataSnapshot.child("marks").getValue(String.class);
                        try {


                            if (finalprn.equals(prn)) {
                                //Toast.makeText(getApplicationContext(), "okay? " + finalprn, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), Student2Activity.class);
                                intent.putExtra("PRNExtra", finalprn);
                                intent.putExtra("nameExtra", finalname);
                                intent.putExtra("usernameExtra", username);
                                intent.putExtra("passwordExtra", password);
                                intent.putExtra("marksExtra", marks);
                                intent.putExtra("WhichActivity", "ThirdParty");
                                finish();
                                startActivity(intent);
                            }
//                            else {
//                                Intent homeIntent = new Intent(getApplicationContext(), MainActivity.class);
//                                startActivity(homeIntent);
//                            }

                        } catch (java.lang.NullPointerException e) {
                            Toast.makeText(getApplicationContext(), "Wrong PRN", Toast.LENGTH_SHORT).show();
//    Intent homeIntent = new Intent(getApplicationContext(), MainActivity.class);
//    startActivity(homeIntent);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });

    }

    public void onBackPressed() {
        finish();
        Intent homeIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(homeIntent);
    }
}
