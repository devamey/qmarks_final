package com.abepro.marksqr;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.MyViewHolder> {
    private List<Student> studentList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView prnNumber;
        public CardView cardView;

        //initialized a viewholder inside its view
        MyViewHolder(View view) {
            super(view);
            prnNumber = view.findViewById(R.id.prnNumber);
            cardView = view.findViewById(R.id.cardView);
        }
    }

    StudentAdapter(Context context, List<Student> studentList) {
        this.context = context;
        this.studentList = studentList;
    }

    //on creation of Recycler View inflate the card layout in the recycler view
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_single_home, parent, false);

        return new MyViewHolder(itemView);
    }

    //diplays the contents in list on specified position
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Student student = studentList.get(position);
        holder.prnNumber.setText(student.getPrnNumber());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, University3Activity.class);
                intent.putExtra("nameExtra", student.getName());
                intent.putExtra("marksExtra", student.getMarks());
                intent.putExtra("key", student.getPassword());
                intent.putExtra("PRNExtra", student.getPrnNumber());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    public void clear() {
        studentList.clear();
        notifyDataSetChanged();
    }
}
