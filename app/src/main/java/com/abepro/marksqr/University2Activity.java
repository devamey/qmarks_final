package com.abepro.marksqr;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class University2Activity extends AppCompatActivity {

    SwipeRefreshLayout swipeRefreshLayout;
    private StudentAdapter studentAdapter;
    DatabaseReference databaseReference;
    private List<Student> studentList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_university2);

        //Firebase database instance
        databaseReference = FirebaseDatabase.getInstance().getReference();
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        swipeRefreshLayout = findViewById(R.id.swipe);
        studentAdapter = new StudentAdapter(this, studentList);
        //Define Recycler View in the layout along with properties
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        //set the layout
        recyclerView.setLayoutManager(mLayoutManager);
        //set to deafult animation type
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        // set the content to the defined adapter
        recyclerView.setAdapter(studentAdapter);

        new GetStudentDetails().execute();
        //implementation of Swipe down to refresh feature
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                        studentAdapter.clear();
                        //swipeRefreshLayout.setEnabled(false);
                        new GetStudentDetails().execute();
                    }
                }, 2500);
            }
        });
    }

    //pull data of Students from firebase database through background async task
    @SuppressLint("StaticFieldLeak")
    private class GetStudentDetails extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //studentadapter is updated dynamically everytime data is changed in database
            studentAdapter.notifyDataSetChanged();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            databaseReference.child("Students").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //Toast.makeText(University2Activity.this, dataSnapshot.toString(), Toast.LENGTH_SHORT).show();
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                        Student student = dataSnapshot1.getValue(Student.class);
                        //Toast.makeText(University2Activity.this, student.getPrnNumber(), Toast.LENGTH_SHORT).show();
                        studentList.add(student);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent homeIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(homeIntent);
    }
}