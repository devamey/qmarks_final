package com.abepro.marksqr;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Student3Activity extends AppCompatActivity {
    private BlowFish blowFish = new BlowFish();

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student3);

        String finalmarks = getIntent().getStringExtra("EncryptedBlowfish");
        String finalprn = getIntent().getStringExtra("prn");
        String finalname = getIntent().getStringExtra("studentname");
        String finalpass = getIntent().getStringExtra("password");

        TextView textView = findViewById(R.id.textViewmarks);
        TextView PRNTextView = findViewById(R.id.prnTextView);
        TextView nameTextView = findViewById(R.id.nameeTextView);
        Button homeb = findViewById(R.id.homeButton);

        homeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                finish();
                startActivity(intent);
            }
        });

        PRNTextView.setText(finalprn);
        nameTextView.setText(finalname);
        String dMarks = blowFish.finalDecrypt(finalpass, finalmarks);
        if (dMarks.equals("faildecryption")) {
            Toast.makeText(this, "Decryption Failed. Kindly scan YOUR QR code only!", Toast.LENGTH_LONG).show();
            finish();
            Intent homeIntent = new Intent(this, MainActivity.class);
            startActivity(homeIntent);
        } else {
//        textView.setText(dMarks);
            String[] semester = dMarks.split("-");
            String sem1 = semester[0];
            String sem2 = semester[1];
            String sem3 = semester[2];
            String sem4 = semester[3];
            String sem5 = semester[4];
            String sem6 = semester[5];
            String sem7 = semester[6];
            String sem8 = semester[7];

            String[] sem1sub = sem1.split(" ");
            String engineeringMathematicsI = sem1sub[0];
            String engineeringGraphics = sem1sub[1];
            String basicCivilEnvironmentalEngineering = sem1sub[2];
            String engineeringChemistry = sem1sub[3];
            String workshop = sem1sub[4];

            String[] sem2sub = sem2.split(" ");
            String engineeringMathematicsII = sem2sub[0];
            String engineeringMechanics = sem2sub[1];
            String basicMechanicalEngineering = sem2sub[2];
            String engineeringPhysics = sem2sub[3];
            String fundamentalsOfProgrammingLanguage = sem2sub[4];

            String[] sem3sub = sem3.split(" ");
            String engineeringMathematicsIII = sem3sub[0];
            String digitalElectronicsLogicDesign = sem3sub[1];
            String dataStructuresandAlgorithms = sem3sub[2];
            String computerOrganization = sem3sub[3];
            String digitalElectronicsLab = sem3sub[4];

            String[] sem4sub = sem4.split(" ");
            String dataStructures = sem4sub[0];
            String microprocessorInterferingTechniques = sem4sub[1];
            String principlesofProgrammingLanguages = sem4sub[2];
            String objectOrientedProgramming = sem4sub[3];
            String computerGraphicsLab = sem4sub[4];

            String[] sem5sub = sem5.split(" ");
            String theoryofComputation = sem5sub[0];
            String dataCommunicationandWirelessSensorNetworks = sem5sub[1];
            String databaseManagementSystemsApplications = sem5sub[2];
            String computerForensicsandCyberApplications = sem5sub[3];
            String programmingLaboratoryI = sem5sub[4];

            String[] sem6sub = sem6.split(" ");
            String principlesofConcurrentandDistributedProgramming = sem6sub[0];
            String embeddedOperatingSystems = sem6sub[1];
            String computerNetworks = sem6sub[2];
            String softwareEngineering = sem6sub[3];
            String programmingLaboratoryII = sem6sub[4];

            String[] sem7sub = sem7.split(" ");
            String designAnalysisofAlgorithms = sem7sub[0];
            String principlesofModernCompilerDesign = sem7sub[1];
            String smartSystemDesignandApplications = sem7sub[2];
            String computerlaboratoryI = sem7sub[3];
            String projectI = sem7sub[4];

            String[] sem8sub = sem8.split(" ");
            String softwareDesignMethodologiesTesting = sem8sub[0];
            String highPerformanceComputing = sem8sub[1];
            String cloudComputing = sem8sub[2];
            String businessIntelligence = sem8sub[3];
            String projectII = sem8sub[4];

            textView.setText(
                    "Semester 1" +
                            "\n\nEngineering Mathematics I: " + engineeringMathematicsI +
                            "\n\nEngineering Graphics: " + engineeringGraphics +
                            "\n\nBasic Civil & Environmental Engineering: " + basicCivilEnvironmentalEngineering +
                            "\n\nEngineering Chemistry: " + engineeringChemistry +
                            "\n\nWorkshop: " + workshop +
                            "\n\nSemester 2" +
                            "\n\nEngineering Mathematics II: " + engineeringMathematicsII +
                            "\n\nEngineering Mechanics: " + engineeringMechanics +
                            "\n\nBasic Mechanical Engineering: " + basicMechanicalEngineering +
                            "\n\nEngineering Physics: " + engineeringPhysics +
                            "\n\nFundamentals of Programming Language: " + fundamentalsOfProgrammingLanguage +

                            "\n\nSemester 3" +
                            "\n\nEngineering Mathematics III: " + engineeringMathematicsIII +
                            "\n\nDigital Electronics & Logic Design: " + digitalElectronicsLogicDesign +
                            "\n\nData Structures and Algorithms: " + dataStructuresandAlgorithms +
                            "\n\nComputer Organization: " + computerOrganization +
                            "\n\nDigital Electronics Lab: " + digitalElectronicsLab +

                            "\n\nSemester 4" +
                            "\n\nData Structures: " + dataStructures +
                            "\n\nMicroprocessor Interfering Techniques: " + microprocessorInterferingTechniques +
                            "\n\nPrinciples of Programming Languages: " + principlesofProgrammingLanguages +
                            "\n\nObject Oriented Programming: " + objectOrientedProgramming +
                            "\n\nComputer Graphics Lab: " + computerGraphicsLab +

                            "\n\nSemester 5" +
                            "\n\nTheory of Computation: " + theoryofComputation +
                            "\n\nDatabase Management Systems Applications: " + databaseManagementSystemsApplications +
                            "\n\nData Communication and Wireless Sensor Networks: " + dataCommunicationandWirelessSensorNetworks +
                            "\n\nComputer Forensic and Cyber Applications: " + computerForensicsandCyberApplications +
                            "\n\nProgramming Laboratory I: " + programmingLaboratoryI +

                            "\n\nSemester 6" +
                            "\n\nPrinciples of Concurrent and Distributed Programming: " + principlesofConcurrentandDistributedProgramming +
                            "\n\nEmbedded Operating Systems: " + embeddedOperatingSystems +
                            "\n\nComputer Networks: " + computerNetworks +
                            "\n\nSoftware Engineering: " + softwareEngineering +
                            "\n\nProgramming Laboratory II: " + programmingLaboratoryII +

                            "\n\nSemester 7" +
                            "\n\nDesign & Analysis of Algorithms: " + designAnalysisofAlgorithms +
                            "\n\nPrinciples of Modern Compiler Design: " + principlesofModernCompilerDesign +
                            "\n\nSmart System Design and Applications: " + smartSystemDesignandApplications +
                            "\n\nComputer laboratory-I: " + computerlaboratoryI +
                            "\n\nProject I: " + projectI +

                            "\n\nSemester 8" +
                            "\n\nSoftware Design Methodologies & Testing: " + softwareDesignMethodologiesTesting +
                            "\n\nHigh Performance Computing: " + highPerformanceComputing +
                            "\n\nCloud Computing: " + cloudComputing +
                            "\n\nBusiness Intelligence: " + businessIntelligence +
                            "\n\nProject II: " + projectII);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent homeIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(homeIntent);
    }
}
