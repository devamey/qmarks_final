package com.abepro.marksqr;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class University3Activity extends AppCompatActivity {

    private Button encrypt;
    private TextView PRNTextView, nameTextView, marksTextView;
    private String gotMarks, gotName, gotPRN, gotKey;
    private BlowFish blowFish = new BlowFish();
    private String QREncryptedMarks;
    private String sem1, sem2, sem3, sem4, sem5, sem6, sem7, sem8;


    private String EngineeringMathematicsI, EngineeringGraphics, BasicCivilEnvironmentalEngineering, EngineeringChemistry, Workshop;
    private String EngineeringMathematicsII, EngineeringMechanics, BasicMechanicalEngineering, EngineeringPhysics, FundamentalsOfProgrammingLanguage;

    private String EngineeringMathematicsIII, DigitalElectronicsLogicDesign, DataStructuresandAlgorithms, ComputerOrganization, DigitalElectronicsLab;
    private String DataStructures, MicroprocessorInterferingTechniques, PrinciplesofProgrammingLanguages, ObjectOrientedProgramming, ComputerGraphicsLab;

    private String TheoryofComputation, DataCommunicationandWirelessSensorNetworks, DatabaseManagementSystemsApplications, ComputerForensicsandCyberApplications, ProgrammingLaboratoryI;
    private String PrinciplesofConcurrentandDistributedProgramming, EmbeddedOperatingSystems, ComputerNetworks, SoftwareEngineering, ProgrammingLaboratoryII;

    private String DesignAnalysisofAlgorithms, PrinciplesofModernCompilerDesign, SmartSystemDesignandApplications, ComputerlaboratoryI, ProjectI;
    private String SoftwareDesignMethodologiesTesting, HighPerformanceComputing, CloudComputing, BusinessIntelligence, ProjectII;

    public SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_university3);

        gotName = getIntent().getStringExtra("nameExtra");
        gotMarks = getIntent().getStringExtra("marksExtra");
        gotPRN = getIntent().getStringExtra("PRNExtra");
        gotKey = getIntent().getStringExtra("key");

//        separated[1]; // this will contain " they taste good"

        PRNTextView = findViewById(R.id.textView8);
        nameTextView = findViewById(R.id.textView9);
        encrypt = findViewById(R.id.button8);
        marksTextView = findViewById(R.id.textView13);
        sharedpreferences = this.getSharedPreferences("TEST", this.MODE_PRIVATE);
        String[] semester = gotMarks.split("-");

        sem1 = semester[0];
        sem2 = semester[1];
        sem3 = semester[2];
        sem4 = semester[3];
        sem5 = semester[4];
        sem6 = semester[5];
        sem7 = semester[6];
        sem8 = semester[7];
        String isit = this.sharedpreferences.getString("which", null);
//        Toast.makeText(this,isit,Toast.LENGTH_SHORT).show();

        if (isit.contains("TP")) {
//(CRASHES HERE) this.sharedpreferences.edit().remove("which").apply();
//                SharedPreferences.Editor editor = sharedpreferences.edit();
//                editor.putString("which","");
//                editor.apply();
//                SharedPreferences.Editor editor = sharedpreferences.edit();
//                editor.clear();
//               editor.apply();
            encrypt.setVisibility(View.INVISIBLE);
        } else {
            encrypt.setVisibility(View.VISIBLE);
//                Toast.makeText(this, isit, Toast.LENGTH_SHORT).show();
        }
//
//
//
//        String isit2= this.sharedpreferences.getString("which",null);
//        Toast.makeText(this,isit2+"2",Toast.LENGTH_SHORT).show();
        String[] sem1sub = sem1.split(" ");

        EngineeringMathematicsI = sem1sub[0];
        EngineeringGraphics = sem1sub[1];
        BasicCivilEnvironmentalEngineering = sem1sub[2];
        EngineeringChemistry = sem1sub[3];
        Workshop = sem1sub[4];

        String[] sem2sub = sem2.split(" ");

        EngineeringMathematicsII = sem2sub[0];
        EngineeringMechanics = sem2sub[1];
        BasicMechanicalEngineering = sem2sub[2];
        EngineeringPhysics = sem2sub[3];
        FundamentalsOfProgrammingLanguage = sem2sub[4];

        String[] sem3sub = sem3.split(" ");

        EngineeringMathematicsIII = sem3sub[0];
        DigitalElectronicsLogicDesign = sem3sub[1];
        DataStructuresandAlgorithms = sem3sub[2];
        ComputerOrganization = sem3sub[3];
        DigitalElectronicsLab = sem3sub[4];

        String[] sem4sub = sem4.split(" ");

        DataStructures = sem4sub[0];
        MicroprocessorInterferingTechniques = sem4sub[1];

        PrinciplesofProgrammingLanguages = sem4sub[2];
        ObjectOrientedProgramming = sem4sub[3];
        ComputerGraphicsLab = sem4sub[4];

        String[] sem5sub = sem5.split(" ");

        TheoryofComputation = sem5sub[0];
        DataCommunicationandWirelessSensorNetworks = sem5sub[1];
        DatabaseManagementSystemsApplications = sem5sub[2];
        ComputerForensicsandCyberApplications = sem5sub[3];
        ProgrammingLaboratoryI = sem5sub[4];

        String[] sem6sub = sem6.split(" ");

        PrinciplesofConcurrentandDistributedProgramming = sem6sub[0];
        EmbeddedOperatingSystems = sem6sub[1];
        ComputerNetworks = sem6sub[2];
        SoftwareEngineering = sem6sub[3];
        ProgrammingLaboratoryII = sem6sub[4];

        String[] sem7sub = sem7.split(" ");

        DesignAnalysisofAlgorithms = sem7sub[0];
        PrinciplesofModernCompilerDesign = sem7sub[1];
        SmartSystemDesignandApplications = sem7sub[2];
        ComputerlaboratoryI = sem7sub[3];
        ProjectI = sem7sub[4];

        String[] sem8sub = sem8.split(" ");

        SoftwareDesignMethodologiesTesting = sem8sub[0];
        HighPerformanceComputing = sem8sub[1];
        CloudComputing = sem8sub[2];
        BusinessIntelligence = sem8sub[3];
        ProjectII = sem8sub[4];


        marksTextView.setText(
                "Semester 1" +
                        "\n\nEngineering Mathematics I: " + EngineeringMathematicsI +
                        "\n\nEngineering Graphics: " + EngineeringGraphics +
                        "\n\nBasic Civil & Environmental Engineering: " + BasicCivilEnvironmentalEngineering +
                        "\n\nEngineering Chemistry: " + EngineeringChemistry +
                        "\n\nWorkshop: " + Workshop +
                        "\n\nSemester 2" +
                        "\n\nEngineering Mathematics II: " + EngineeringMathematicsII +
                        "\n\nEngineering Mechanics: " + EngineeringMechanics +
                        "\n\nBasic Mechanical Engineering: " + BasicMechanicalEngineering +
                        "\n\nEngineering Physics: " + EngineeringPhysics +
                        "\n\nFundamentals of Programming Language: " + FundamentalsOfProgrammingLanguage +

                        "\n\nSemester 3" +
                        "\n\nEngineering Mathematics III: " + EngineeringMathematicsIII +
                        "\n\nDigital Electronics & Logic Design: " + DigitalElectronicsLogicDesign +
                        "\n\nData Structures and Algorithms: " + DataStructuresandAlgorithms +
                        "\n\nComputer Organization: " + ComputerOrganization +
                        "\n\nDigital Electronics Lab: " + DigitalElectronicsLab +

                        "\n\nSemester 4" +
                        "\n\nData Structures: " + DataStructures +
                        "\n\nMicroprocessor Interfering Techniques: " + MicroprocessorInterferingTechniques +
                        "\n\nPrinciples of Programming Languages: " + PrinciplesofProgrammingLanguages +
                        "\n\nObject Oriented Programming: " + ObjectOrientedProgramming +
                        "\n\nComputer Graphics Lab: " + ComputerGraphicsLab +

                        "\n\nSemester 5" +
                        "\n\nTheory of Computation: " + TheoryofComputation +
                        "\n\nDatabase Management Systems Applications: " + DatabaseManagementSystemsApplications +
                        "\n\nData Communication and Wireless Sensor Networks: " + DataCommunicationandWirelessSensorNetworks +
                        "\n\nComputer Forensic and Cyber Applications: " + ComputerForensicsandCyberApplications +
                        "\n\nProgramming Laboratory I: " + ProgrammingLaboratoryI +

                        "\n\nSemester 6" +
                        "\n\nPrinciples of Concurrent and Distributed Programming: " + PrinciplesofConcurrentandDistributedProgramming +
                        "\n\nEmbedded Operating Systems: " + EmbeddedOperatingSystems +
                        "\n\nComputer Networks: " + ComputerNetworks +
                        "\n\nSoftware Engineering: " + SoftwareEngineering +
                        "\n\nProgramming Laboratory II: " + ProgrammingLaboratoryII +

                        "\n\nSemester 7" +
                        "\n\nDesign & Analysis of Algorithms: " + DesignAnalysisofAlgorithms +
                        "\n\nPrinciples of Modern Compiler Design: " + PrinciplesofModernCompilerDesign +
                        "\n\nSmart System Design and Applications: " + SmartSystemDesignandApplications +
                        "\n\nComputer laboratory-I: " + ComputerlaboratoryI +
                        "\n\nProject I: " + ProjectI +

                        "\n\nSemester 8" +
                        "\n\nSoftware Design Methodologies & Testing: " + SoftwareDesignMethodologiesTesting +
                        "\n\nHigh Performance Computing: " + HighPerformanceComputing +
                        "\n\nCloud Computing: " + CloudComputing +
                        "\n\nBusiness Intelligence: " + BusinessIntelligence +
                        "\n\nProject II: " + ProjectII);


        PRNTextView.setText(gotPRN);
        nameTextView.setText(gotName);
//        marksTextView.setText(gotMarks);

        QREncryptedMarks = blowFish.finalEncrypt(gotKey, gotMarks);

        final Intent university4intent = new Intent(this, University4Activity.class);
        university4intent.putExtra("ENCRYPTEDQRMARKS", QREncryptedMarks);
        encrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                university4intent.putExtra("PRNExtra", gotPRN);
                startActivity(university4intent);
                finish();
            }
        });

    }
}
