package com.abepro.marksqr;

import android.util.Log;

import java.security.GeneralSecurityException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class BlowFish {
    private final static String ALGORITM = "Blowfish";
    private final static String KEY = "key_to_be_passed";
    private final static String PLAIN_TEXT = "here is your text";
    private String encryptedMarks, decrypted;

    public void run() {
        try {
            byte[] encrypted = encrypt(KEY, PLAIN_TEXT);
            Log.i("FOO", "Encrypted: " + bytesToHex(encrypted));
            String decrypted = decrypt(KEY, encrypted);
            Log.i("FOO", "Decrypted: " + decrypted);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
    }

    public String finalEncrypt(String bkey, String btext) {
        try {
            byte[] encrypted = encrypt(bkey, btext);
            encryptedMarks = bytesToHex(encrypted);
            Log.i("FOO", "Encrypted: " + encryptedMarks);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            return "failencryption";
        }
        return encryptedMarks;
    }

    public String finalDecrypt(String bkey, String toDecryptMarks) {
        try {
            byte[] finaltodecrypt = hexStringToByteArray(toDecryptMarks);
            decrypted = decrypt(bkey, finaltodecrypt);
            Log.i("FOO", "Decrypted: " + decrypted);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            return "faildecryption";
        }
        return decrypted;
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    private byte[] encrypt(String key, String plainText) throws GeneralSecurityException {
        SecretKey secret_key = new SecretKeySpec(key.getBytes(), ALGORITM);
        Cipher cipher = Cipher.getInstance(ALGORITM);
        cipher.init(Cipher.ENCRYPT_MODE, secret_key);
        return cipher.doFinal(plainText.getBytes());
    }

    private String decrypt(String key, byte[] encryptedText) throws GeneralSecurityException {
        SecretKey secret_key = new SecretKeySpec(key.getBytes(), ALGORITM);
        Cipher cipher = Cipher.getInstance(ALGORITM);
        cipher.init(Cipher.DECRYPT_MODE, secret_key);
        byte[] decrypted = cipher.doFinal(encryptedText);
        return new String(decrypted);
    }

    private static String bytesToHex(byte[] data) {
        if (data == null)
            return null;
        StringBuilder str = new StringBuilder();
        for (byte aData : data) {
            if ((aData & 0xFF) < 16)
                str.append("0").append(Integer.toHexString(aData & 0xFF));
            else
                str.append(Integer.toHexString(aData & 0xFF));
        }
        return str.toString();
    }
}
