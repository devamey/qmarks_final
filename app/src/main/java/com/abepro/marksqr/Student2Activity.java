package com.abepro.marksqr;

import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

public class Student2Activity extends AppCompatActivity implements QRCodeReaderView.OnQRCodeReadListener {
    private QRCodeReaderView qrCodeReaderView;
    private String password, studentName, prn, userName, whichactivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student2);

        userName = this.getIntent().getStringExtra("usernameExtra");
        password = this.getIntent().getStringExtra("passwordExtra");
        studentName = this.getIntent().getStringExtra("nameExtra");
        prn = this.getIntent().getStringExtra("PRNExtra");
        whichactivity = this.getIntent().getStringExtra("WhichActivity");

        Button button = findViewById(R.id.button6);
        qrCodeReaderView = findViewById(R.id.qrdecoderview);
        qrCodeReaderView.setOnQRCodeReadListener(this);

        final Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.putExtra("LOGINAS", "university");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(loginIntent);
            }
        });
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        Intent student3Intent = new Intent(this, Student3Activity.class);
        student3Intent.putExtra("EncryptedBlowfish", text);
        student3Intent.putExtra("password", password);
        student3Intent.putExtra("studentname", studentName);
        student3Intent.putExtra("prn", prn);
        finish();
        startActivity(student3Intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        qrCodeReaderView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        qrCodeReaderView.stopCamera();
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        Intent thirdPartyIntent = new Intent(getApplicationContext(), ThirdPartyActivity.class);
        if (whichactivity.equals("ThirdParty")) {
            startActivity(thirdPartyIntent);
            getIntent().removeExtra("WhichActivity");
        } else
            startActivity(loginIntent);
    }

}
