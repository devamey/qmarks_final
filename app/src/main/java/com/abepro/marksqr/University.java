package com.abepro.marksqr;

/**
 * Created by Chaitanya on 2/17/2018.
 */

/*Pojo Class*/

public class University {
    String username, password;

    public University() {
    }

    public University(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
