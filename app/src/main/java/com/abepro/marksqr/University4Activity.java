package com.abepro.marksqr;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.sumimakito.awesomeqr.AwesomeQRCode;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.UUID;

public class University4Activity extends AppCompatActivity {
    //declaration
    private Button uploadButton;
    private ImageView imageView;
    private String PRNExtra;
    private ProgressBar progressBar;
    //Firebase Storage Initialization
    //Firebase Storage instance created
    FirebaseStorage storage = FirebaseStorage.getInstance();
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_university4);
        //progress bar defined
        progressBar=findViewById(R.id.progressBar3);
        //set progress bar to invisible
        progressBar.setVisibility(View.INVISIBLE);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        PRNExtra = this.getIntent().getStringExtra("PRNExtra");
        String finalEncryptedMarks = getIntent().getStringExtra("ENCRYPTEDQRMARKS");
        final Intent homeIntent=new Intent(getApplicationContext(),MainActivity.class);
        uploadButton = findViewById(R.id.button9);
        imageView = findViewById(R.id.imageView);
        //converting image to bitmap format
        Bitmap logoImage = BitmapFactory.decodeResource(this.getResources(), R.drawable.sppu);
        Bitmap bgimage = BitmapFactory.decodeResource(this.getResources(), R.drawable.bg2);
        Toast.makeText(getApplicationContext(), "QR generated successfully!", Toast.LENGTH_SHORT).show();
        //QRcode generated with properties
        new AwesomeQRCode.Renderer()
                .logo(logoImage)
                .logoScale(0.3f)
                .background(bgimage)
                .autoColor(true)
                .roundedDots(true)
//                .colorDark(Color.rgb(255,110,1))
//                .colorLight(Color.YELLOW)
                .dotScale(0.8f)
                .contents(finalEncryptedMarks)
                .size(800).margin(20)
                .renderAsync(new AwesomeQRCode.Callback() {
                    @Override
                    public void onRendered(AwesomeQRCode.Renderer renderer, final Bitmap bitmap) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // Tip: here we use runOnUiThread(...) to avoid the problems caused by operating UI elements from a non-UI thread.

                                // Create a storage reference from our app


                                imageView.setImageBitmap(bitmap);
                            }
                        });
                        //upload to firebase storage
                        uploadButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                progressBar.setVisibility(View.VISIBLE);
                                StorageReference storageRef = storage.getReference();
                                String uniqueId = UUID.randomUUID().toString() + ".jpg";

// Create a reference to "mountains.jpg"
                                StorageReference mountainsRef = storageRef.child(uniqueId);

// Create a reference to 'images/mountains.jpg'
                                StorageReference mountainImagesRef = storageRef.child("images/" + uniqueId);

// While the file names are the same, the references point to different files
                                mountainsRef.getName().equals(mountainImagesRef.getName());    // true
                                mountainsRef.getPath().equals(mountainImagesRef.getPath());    // false

                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                                byte[] data = baos.toByteArray();
                                UploadTask uploadTask = mountainsRef.putBytes(data);
                                uploadTask.addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {
                                        // Handle unsuccessful uploads
                                    }
                                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                        String url = downloadUrl.toString();
                                        databaseReference.child("Students").child(PRNExtra).child("qR").setValue(url);
                                        Log.e("URL is ", url);
                                        progressBar.setVisibility(View.INVISIBLE);
                                        Toast.makeText(getApplicationContext(), url, Toast.LENGTH_SHORT).show();
                                        finish();
                                        startActivity(homeIntent);

                                    }
                                });
                            }
                        });
                    }


                    //set progress bar to invisible on occurrence of error
                    @Override
                    public void onError(AwesomeQRCode.Renderer renderer, Exception e) {
                        progressBar.setVisibility(View.INVISIBLE);
                        startActivity(homeIntent);
                        e.printStackTrace();
                    }
                });

    }

}
