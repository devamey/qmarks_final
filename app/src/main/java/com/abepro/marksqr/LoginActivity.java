package com.abepro.marksqr;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {
    private EditText editTextUserName, editTextPassword;
    private String username, password, checkLogin;
    private String item = "";
    private boolean item1 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final SharedPreferences sharedpreferences;
        checkLogin = getIntent().getStringExtra("LOGINAS");

        editTextUserName = findViewById(R.id.apd);
        editTextPassword = findViewById(R.id.editText2);
        Button loginButton = findViewById(R.id.button4);
        sharedpreferences = getSharedPreferences("TEST", MODE_PRIVATE);

        final Intent student2Intent, university2Intent, ThirdPartyIntent;
        //Create Firebase database reference here
        final DatabaseReference dbref = FirebaseDatabase.getInstance().getReference();

        ThirdPartyIntent = new Intent(this, ThirdPartyActivity.class);
        student2Intent = new Intent(this, Student2Activity.class);
        university2Intent = new Intent(this, University2Activity.class);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = editTextUserName.getText().toString().trim();
                password = editTextPassword.getText().toString().trim();
                if (checkLogin.contains("student")) {
                    //Connecting the database reference to the child(Student) values
                    dbref.child("Students").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            //traverse through the registered students to get username and password
                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                //get instance of Student pojo class
                                Student student = dataSnapshot1.getValue(Student.class);
                                if (student.getUsername().equals(username) && student.getPassword().equals(password)) {
                                    item1 = true;
                                    item = "Logged in as " + dataSnapshot1.getKey();
                                    Log.v("asd", item + "\t" + student.getUsername());
                                    //pass values along with intent
                                    student2Intent.putExtra("PRNExtra", student.getPrnNumber());
                                    student2Intent.putExtra("nameExtra", student.getName());
                                    student2Intent.putExtra("usernameExtra", student.getUsername());
                                    student2Intent.putExtra("passwordExtra", student.getPassword());
                                    student2Intent.putExtra("marksExtra", student.getMarks());
                                    startActivity(student2Intent);
                                    finish();
                                    break;
                                    //validation of text entered
                                } else if (editTextUserName.getText().toString().equals("") || editTextPassword.getText().toString().equals("")) {
                                    item1 = false;
                                    item = "Credentials should not be Empty";
                                } else if (editTextPassword.getText().toString().length() >= 15 || editTextPassword.getText().toString().length() <= 8) {
                                    item1 = false;
                                    item = "Password length must be of 8-15 characters";
                                } else {
                                    item1 = false;
                                    item = "Login Failed, Check Credentials";
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                } else if (checkLogin.contains("university")) {
                    //Connecting the database reference to the child(University) values
                    dbref.child("University").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            //traverse through the registered universities to get username and password
                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                //get instance of University pojo class
                                University university = dataSnapshot1.getValue(University.class);
                                if (university.getUsername().equals(username) && university.getPassword().equals(password)) {
                                    item1 = true;
                                    item = "Logged in as " + dataSnapshot1.getKey();
                                    Log.v("asd", item + "\t" + university.getUsername());
                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.putString("which", "");
                                    editor.apply();
                                    //pass values along with intent
                                    university2Intent.putExtra("PRNExtra", item);
                                    startActivity(university2Intent);
                                    finish();
                                    break;
                                    //Validation of text entered
                                } else if (editTextUserName.getText().toString().equals("") || editTextPassword.getText().toString().equals("")) {
                                    item1 = false;
                                    item = "Credentials should not be Empty";
                                } else if (editTextPassword.getText().toString().length() >= 15 || editTextPassword.getText().toString().length() <= 8) {
                                    item1 = false;
                                    item = "Password length must be of 8-15 characters";
                                } else {
                                    item1 = false;
                                    item = "Login Failed, Check Credentials";
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    if (item1 == true) {
                        //Toast.makeText(LoginActivity.this, "Logged in as " + item, Toast.LENGTH_SHORT).show();
                    } else if (item.equals("Credentials should not be Empty")) {
                        Toast.makeText(LoginActivity.this, item, Toast.LENGTH_SHORT).show();
                    } else if (item.equals("Password length must be of 8-15 characters")) {
                        Toast.makeText(LoginActivity.this, item, Toast.LENGTH_SHORT).show();
                    } else if (item.equals("Login Failed, Check Credentials")) {
                        Toast.makeText(LoginActivity.this, item, Toast.LENGTH_SHORT).show();
                    }
                } else if (checkLogin.contains("thirdparty")) {
                    //Connecting the database reference to the child(Third Party) values
                    dbref.child("ThirdParty").addValueEventListener(new ValueEventListener() {
                        //traverse through the registered universities to get username and password
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                                //get instance of University pojo class
                                University university = dataSnapshot1.getValue(University.class);
                                if (university.getUsername().equals(username) && university.getPassword().equals(password)) {
                                    item1 = true;
                                    item = dataSnapshot1.getKey();
                                    Log.v("asd", item + "\t" + university.getUsername());
                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.putString("which", "TP");
                                    editor.apply();
//                                    university2Intent.putExtra("PRNExtra", item);
//                                    startActivity(university2Intent);
                                    startActivity(ThirdPartyIntent);
                                    finish();
                                    break;
                                    //Validation of text entered
                                } else if (editTextUserName.getText().toString().equals("") || editTextPassword.getText().toString().equals("")) {
                                    item1 = false;
                                    item = "Credentials should not be Empty";
                                } else if (editTextPassword.getText().toString().length() >= 15 || editTextPassword.getText().toString().length() <= 8) {
                                    item1 = false;
                                    item = "Password length must be of 8-15 characters";
                                } else {
                                    item1 = false;
                                    item = "Login Failed, Check Credentials";
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    //Validation of text entered
                    if (item1 == true) {
                        //Toast.makeText(LoginActivity.this, "Logged in as " + item, Toast.LENGTH_SHORT).show();
                    } else if (item.equals("Credentials should not be Empty")) {
                        Toast.makeText(LoginActivity.this, item, Toast.LENGTH_SHORT).show();
                    } else if (item.equals("Password length must be of 8-15 characters")) {
                        Toast.makeText(LoginActivity.this, item, Toast.LENGTH_SHORT).show();
                    } else if (item.equals("Login Failed, Check Credentials")) {
                        Toast.makeText(LoginActivity.this, item, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        Button homeBtn = findViewById(R.id.button5);
        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent homeIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(homeIntent);
    }
}