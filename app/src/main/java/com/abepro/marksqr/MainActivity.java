package com.abepro.marksqr;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.yanzhenjie.permission.Action;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.Permission;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    //Initialization
    Button studentLogin, universityLogin, thirdPartyLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Declaration and connecting with xml
        studentLogin = findViewById(R.id.button);
        universityLogin = findViewById(R.id.button2);
        thirdPartyLogin = findViewById(R.id.button3);
        //declaring intent
        final Intent loginIntent = new Intent(this, LoginActivity.class);
        //set on click action to the designated buton
        studentLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginIntent.putExtra("LOGINAS", "student");
                startActivity(loginIntent);
                finish();
            }
        });

        //set on click action to the designated buton
        universityLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginIntent.putExtra("LOGINAS", "university");
                startActivity(loginIntent);
                finish();
            }
        });

        //set on click action to the designated buton
        thirdPartyLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginIntent.putExtra("LOGINAS", "thirdparty");
                startActivity(loginIntent);
            }
        });
        AndPermission.with(this)
                .permission(Permission.CAMERA)
                .onGranted(new Action() {
                    @Override
                    public void onAction(List<String> permissions) {
                    }
                }).onDenied(new Action() {
            @Override
            public void onAction(List<String> permissions) {
                Toast.makeText(getApplicationContext(), "Give CAMERA permission to scan QR code.", Toast.LENGTH_SHORT).show();
            }
        })
                .start();
    }

    @Override
    public void onBackPressed() {
        super.finish();
    }

}
