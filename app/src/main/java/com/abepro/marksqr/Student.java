package com.abepro.marksqr;

/**
 * Created by Chaitanya on 2/17/2018.
 */
/*Pojo Class*/

public class Student {
    private String qR, username, password, marks, prnNumber, name;

    public Student() {
        //default constructor
    }

    public Student(String qR, String username, String password, String marks, String prnNumber, String name) {
        //parametrised constructor
        this.qR = qR;
        this.username = username;
        this.password = password;
        this.marks = marks;
        this.prnNumber = prnNumber;
        this.name = name;

    }

    //Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrnNumber() {
        return prnNumber;
    }

    public void setPrnNumber(String prnNumber) {
        this.prnNumber = prnNumber;
    }

    public String getqR() {
        return qR;
    }

    public void setqR(String qR) {
        this.qR = qR;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }
}
